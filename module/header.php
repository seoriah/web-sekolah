<?php
	//include 'dashboard.php';
	//header("location: index.php?page=main");
	//error_reporting(0);
	include("include/mysql.php");
	include("include/date.php");
?>

<!DOCTYPE HTML>
<!--
	Ion by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>SMK Islam Al-Munir</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header" class="skel-layers-fixed">
				
				<h1><a href="./">Al-Munir</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="./">Beranda</a></li>
						
						<li><a href="?info">Info</a></li>
						<?php 
							$data = $db->query("SELECT * FROM page");
							
							while ($rows = $data->fetch_array()) {
								echo "<li>
										<a href=\"?page=$rows[page_id]\">$rows[page_title]</a>
									</li>";
							}
						?>
						
						<li><a href="?buku-tamu">Buku Tamu</a></li>
					</ul>
				</nav>
			</header>

