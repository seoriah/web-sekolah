<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<div class="row double">
						<div class="6u">
							<div class="row collapse-at-2">
								<div class="6u">
									<h3>LOKASI</h3>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.457916503617!2d113.28323921418142!3d-7.188475072548987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7872c0202f165%3A0x5f43d89e323a071f!2sSMK+Al-Munir!5e0!3m2!1sid!2sid!4v1514227305766" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
								
							</div>
						</div>
						<div class="6u">
							<h2>KATA BIJAK PENDIDIKAN</h2>
							<p>Filosofi dari pendidikan saat ini akan menjadi filosofi pemerintahan dimasa yang akan datang.</p>
							<ul class="icons">
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
							</ul>
						</div>
					</div>
					<ul class="copyright">
						<li>&copy;All rights reserved.</li>
						<li>2017</li>
					</ul>
				</div>
			</footer>

	</body>
</html>