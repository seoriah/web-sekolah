<?php

	$page 	= $_GET['info'];
	$data 	= $db->query("SELECT * FROM info ORDER BY info_tanggal DESC LIMIT 3");
?>
	<!-- Main -->
			<section id="main" class="wrapper style1">
			
				<div class="container">
					<div class="row">
						<div class="8u">
							<?php while ($row = $data->fetch_array()){

								$admin = mysqli_fetch_array($db->query("SELECT admin_name FROM admin WHERE admin_id=$row[admin_id]"));
								echo "<section>
								<center><h2><a href='?info-detail=".$row['info_id']."'>".$row['info_title']."</a></h2>
								<h6>".idDate($row['info_tanggal']).", Oleh : ".$admin['admin_name']."</h5>
								<p>".substr($row['info_content'], 0,300)." ...</p></center>
								</section>
								<hr class=\"major\" />";
							} ?>
						</div>
						<div class="4u">
							<section>
								<h3>INFO TERKINI</h3>
								<?php 
								$data  = mysqli_fetch_array($db->query("SELECT * FROM info ORDER BY info_tanggal DESC LIMIT 1"));
									echo "<a href='?info-detail=".$data['info_id']."'>".$data['info_title']."</a>
										<p>".substr($data['info_content'], 0, 150)." ...</p>";
								?>
							</section>
							<hr />
							<section>
								<h3>INFO LAINNYA</h3>
								<ul class="alt">
									<?php 
										$rs = $db->query("SELECT *  FROM info ORDER BY info_tanggal ASC LIMIT 5");
										while($rw = $rs->fetch_array()){
											echo "<li>
												<a href='?info-detail=".$rw['info_id']."'>".$rw['info_title']."</a>
												</li>";
										}
									?>
								</ul>
							</section>
						</div>
					</div>
					<hr class="major" />
				
				</div>
			</section>