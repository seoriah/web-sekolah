<?php
	$page 	= $_GET['page'];
	$data 	= $db->query("SELECT * FROM page WHERE page_id=$page");
	$row	= $data->fetch_array();
?>
			<section id="main" class="wrapper style1">
				<header class="major">
					<h2><?php echo $row['page_title']?></h2>
					<p>SMK ISLAM AL-MUNIR</p>
				</header>
				<div class="container">
					<section>
						
						<p><?php echo $row['page_content']?></p>
					</section>
					<hr class="major" />
					
				</div>
			</section>
